module.exports = {
  arrowParens: "always",
  overrides: [
    {
      files: "LICENSE",
      options: { parser: "markdown" },
    },
    {
      files: ["*.latte", "*.xml", "*.xml.dist"],
      options: {
        parser: "html",
        printWidth: 178,
      },
    },
  ],
  printWidth: 88,
  quoteProps: "consistent",
  tabWidth: 2,
  trailingComma: "all",
};
