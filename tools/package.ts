import { existsSync, mkdirSync, readFileSync, writeFileSync } from "fs";
import { resolve as $R } from "path";
import glob from "glob";
import {ncp} from "ncp";
import UglifyJS from "uglify-js";
import packageJson from "../package.json";

const output = $R(__dirname, "../dist");

if (!existsSync(output)) {
  mkdirSync(output, { recursive: true });
}

[".gitignore", "bin", "README.md", "CHANGELOG.md", "LICENSE"].forEach((path) => {
  const copy = $R(__dirname, "..", path);

  if (existsSync(copy)) {
    ncp(copy, $R(output, path), () => null);
  }
});

const json = {
  ...packageJson,

  "scripts": undefined,
  "devDependencies": undefined,
  "husky": undefined,
  "lint-staged": undefined,
};

writeFileSync($R(output, "package.json"), JSON.stringify(json, null, 2));

glob(`${output}/**/*.js`, (err, matches) => {
  matches.forEach((file) => {
    const { code } = UglifyJS.minify(readFileSync(file, "utf8"), {
      output: {
        comments: /.+map$/,
      },
    });

    writeFileSync(file, code);
  });
});
