module.exports = {
  changedSince: "HEAD",
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: "coverage",
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
  modulePathIgnorePatterns: ["<rootDir>/dist"],
  silent: true,
  testPathIgnorePatterns: ["/dist/", "/docs/", "/examples/", "/node_modules/"],
  transform: { "^.+\\.(ts|tsx)$": "ts-jest" },
};
