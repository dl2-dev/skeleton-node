# Changelog

## [Unreleased]

Not yet released; provisionally 2.0.0 (may change).

### Added

### Changed

### Removed

[unreleased]: https://bitbucket.org/dl2-dev/ts-utils/branches/compare/HEAD..1.0.0
